CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Recommended libraries
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
 * Delta Drupal theme is fully responsive and fits to all screen sizes. 
   OS Delta is not dependent on any core theme, has modern and clean design. 
   Theme is perfect for any kind of portfolio and personal website,
   corporate and company site or other small business website.

REQUIREMENTS
------------
This theme requires the following modules:
 * Libraries API (https://www.drupal.org/project/libraries)
 * Admin menu (https://www.drupal.org/project/admin_menu)
 * Advanced help (https://www.drupal.org/project/advanced_help)
 * Contact form blocks (https://www.drupal.org/project/contact_form_blocks)
 * Ctools (https://www.drupal.org/project/ctools)
 * Imageblock (https://www.drupal.org/project/imageblock)
 * Media (https://www.drupal.org/project/media)
 * Multiform (https://www.drupal.org/project/multiform)
 * Page 404 (https://www.drupal.org/project/navigation404)
 * Wysiwyg (https://www.drupal.org/project/wysiwyg)

This theme requires the following libraries:
 * Tubular (https://code.google.com/p/jquery-tubular/downloads/list)
 * Viewportchecker (https://github.com/dirkgroenen/jQuery-viewport-checker)

RECOMMENDED MODULES
-------------------
 We recommended for this theme the following modules:
 * Views (https://drupal.org/project/views)
 * Panels (https://drupal.org/project/panels)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * You may want to disable Toolbar module, since its output clashes with
   Administration Menu.
 * Unpack the libraries and place it folders your_site/sites/all/libraris.

CONFIGURATION
-------------
 * To customize a template, you will need to path
   admin panel -> appearance -> settings -> os_delta. In this directory are
    located our  settings to help you customize the template to your liking.

TROUBLESHOOTING
---------------
 * If you have any questions, refer to "MAINTAINERS"

FAQ
---
 * To create a template OS Delta We offer you to create the following:
 * Content type Features:
   You need to go to the admin panel 
   structure/content types/add new content types
   Create this content type (name: Features) machine name (featurs),
   and add to it the following fields:
   icons - machine name - field_icon_name_featurs;
   title - machine name - title;
   body - machine name -  field_body_featurs;

 * Content type Home Page Gallery:
   You need to go to the admin panel 
   structure/content types/add new content types
   Create this content type (name: Home Page Gallery) 
   machine name (home_page_gallery), add to it the following fields: 
   title - machine name - title;
   optional description - machine name - field_optdesc;
   images - machine name - field_images;

 * Content type Our Team:
   You need to go to the admin panel 
   structure/content types/add new content types
   Create this content type (name: Our Team) 
   machine name (our_team), and add to it the following fields: 
   title - machine name - title;
   position - machine name - field_position;
   description - machine name - field_description;
   imeges - machine name - field_imeges;
   twitter URL - machine name - field_twitter_url;
   facebook URL - machine name - field_facebook_url;
   flickr URL - machine name - field_flickr_url;
   linkedin URL - machine name - field_linkedin_url;
   youTube URL - machine name - field_youtube_url;
   pinterest URL - machine nameE - field_pinterest_url;
   google+ URL - machine name - field_google_url;
   dribbble URL - machine name - field_dribbble_url;
   vimeo URL - machine name - field_vimeo_url;
   instagram URL - machine name - field_instagram_url;
   vk URL - machine name - field_vk_url;

MAINTAINERS
-----------
 * This template has been developed by a team 
   Ordasoft - http://ordasoft.com 
   Ordasoft offers a large number of templates to suit
   your taste. Support forum - http://ordasoft.com/Forum/
   We also offer detailed information on this template
   http://ordasoft.com/drupal-themes-documentation/delta/index.html
